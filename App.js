import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, Text, StatusBar} from 'react-native';
import GPSState from 'react-native-gps-state';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gpsState: 0,
    };
  }

  componentDidMount() {
    GPSState.addListener((status) => {
      console.log({status});
      switch (status) {
        case GPSState.NOT_DETERMINED:
          console.log('NOT_DETERMINED');
          this.setState({gpsState: 'NOT_DETERMINED'});
          break;

        case GPSState.RESTRICTED:
          console.log('RESTRICTED');
          this.setState({gpsState: 'RESTRICTED'});
          // GPSState.openLocationSettings();
          break;

        case GPSState.DENIED:
          console.log('DENIED');
          this.setState({gpsState: 'DENIED'});
          break;

        case GPSState.AUTHORIZED_ALWAYS:
          console.log('AUTHORIZED_ALWAYS');
          this.setState({gpsState: 'AUTHORIZED_ALWAYS'});
          //TODO do something amazing with you app
          break;

        case GPSState.AUTHORIZED_WHENINUSE:
          console.log('AUTHORIZED_WHENINUSE');
          this.setState({gpsState: 'AUTHORIZED_WHENINUSE'});
          //TODO do something amazing with you app
          break;
      }
    });
    GPSState.requestAuthorization(GPSState.AUTHORIZED_ALWAYS);
  }

  componentWillUnmount() {
    console.log('remove');
    GPSState.removeListener();
  }

  render() {
    const {gpsState} = this.state;
    return (
      <>
        <SafeAreaView>
          <Text>{gpsState}</Text>
        </SafeAreaView>
      </>
    );
  }
}
